package com.lmccarty.trailersearch.activity.test;

import com.lmccarty.trailersearch.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.support.v4.widget.DrawerLayout;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.lmccarty.trailersearch.activity.MainActivity;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity activity;
	private String[] drawerList;
	private ActionBar actionbar;
	private DrawerLayout drawerLayout;
	private ListView drawerListView;
	private ListAdapter adapter;

	public MainActivityTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		activity = getActivity();

		drawerList = activity.getResources().getStringArray(R.array.nav_drawer_array);

		actionbar = (ActionBar) activity.getActionBar();

		drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

		drawerListView = (ListView) activity.findViewById(R.id.left_drawer);

		adapter = drawerListView.getAdapter();
	}

	@MediumTest
	public void testPreConditions() {
		assertNotNull("MainActivity is null", activity);
		assertNotNull("ActionBar is null", actionbar);
		assertNotNull("DrawerLayout is null", drawerLayout);
		assertNotNull("ListView is null", drawerListView);
		assertNotNull("Drawer list array is null", drawerList);
		assertTrue(drawerListView.getOnItemClickListener() != null);
		assertTrue(adapter != null);
		assertEquals(3, adapter.getCount());
	}

	@MediumTest
	public void testMainActivityActionBar_labelText() {
		final String expected = activity.getString(R.string.app_name);
		final String actual = actionbar.getTitle().toString();

		assertEquals(expected, actual);
	}

	@MediumTest
	public void testNavDrawer_labelText() {
		final String expectedNav1 = activity.getString(R.string.dvd_releases);
		final String expectedNav2 = activity.getString(R.string.fav_movies);
		final String expectedNav3 = activity.getString(R.string.fav_actors);

		final String actualNav1 = drawerListView.getItemAtPosition(0).toString();
		final String actualNav2 = drawerListView.getItemAtPosition(1).toString();
		final String actualNav3 = drawerListView.getItemAtPosition(2).toString();

		assertEquals(expectedNav1, actualNav1);
		assertEquals(expectedNav2, actualNav2);
		assertEquals(expectedNav3, actualNav3);
	}

	@MediumTest
	public void testMenu_enterSearchTerms() {
		activity.openOptionsMenu();

		ActivityMonitor monitor = getInstrumentation().addMonitor(
				MainActivity.class.getName(), null, false);

		getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
		getInstrumentation().invokeMenuActionSync(activity, R.id.action_search_title, 0);
	}
}
